from MatrixClass import Matrix

def solve():
    list = []
    with open("exp1.txt", "r") as file:
        for line in file:
            parts = line.split(",")
            parts[0] = float(parts[0])
            parts[1] = float(parts[1].strip())
            list.append(parts)
    # print('Original matrix: ', list)
    theta = gradientDescent(list, 0.01, 1500)
    print(theta)
    print('Predicted income for 35k people', (theta[0] + theta[1] * 3.5) * 10000)
    print('Predicted income for 70k people', (theta[0] + theta[1] * 7) * 10000)
    # mat = Matrix([[3], [2], [4], [1]])
    # print(mat ** 2)
    # print(Matrix.transpose(mat))

def addOneColumn(list):
    result = []
    for i in list:
        result.append([1, i[0]])

    return result

def getYvalues(list):
    newList = []
    for x in list:
        newList.append([x[1]])    

    return newList

def showX(list):
    newList = []
    for x in list:
        newList.append(x[0])

    return newList

def summation(training_data, theta0, theta1, theta_index):
    sum = 0

    # [[0],[0]]
    thetaMatrix = Matrix([[theta0],[theta1]])    
    # [[1, datax], [1, datax] ...] 
    dataXwithOnes = Matrix(addOneColumn(training_data))
    yData = Matrix(getYvalues(training_data))
    xData = showX(training_data)
    x = Matrix([xData])
    errors = (dataXwithOnes * thetaMatrix) - yData

    if theta_index == 1:
        errors = x * errors

    for element in errors.value:
        sum += element[0]
    
    return sum

def gradientDescent(training_data, learning_rate, iterations):
    theta0 = 0
    theta1 = 0
    m = len(training_data)

    for i in range(0, iterations):

        temp0 = theta0 - (learning_rate/m) * summation(training_data, theta0, theta1, 0)
        temp1 = theta1 - (learning_rate/m) * summation(training_data, theta0, theta1, 1)
 
        theta0 = temp0
        theta1 = temp1
    
    return(theta0, theta1)

solve()